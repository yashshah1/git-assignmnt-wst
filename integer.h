typedef struct Integer
{
	int i;
	struct Integer *p;
	struct Integer *q;
}Integer;

void initInteger(Integer *a);
void addDigit(Integer *a, char c);
Integer createIntegerFromString(char *str);
Integer addIntegers(Integer a, Integer b);
Integer substractIntegers(Integer a, Integer b);
void printInteger(Integer a);
void destroyInteger(Integer *a);